package books.service;

import books.model.Author;
import books.model.Book;

import java.util.List;

public interface AuthorService {
    void saveOrUpdate(Author author, Book book);
    void delete(Author author);
    List<Author> getAll();
}