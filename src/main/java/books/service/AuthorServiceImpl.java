package books.service;

import books.dao.AuthorDao;
import books.dao.AuthorDaoImpl;
import books.model.Author;
import books.model.Book;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.util.ArrayList;
import java.util.List;

@Service
@ManagedBean(name="authorService")
@SessionScoped
public class AuthorServiceImpl implements AuthorService {
    private static final Logger logger = Logger.getLogger(AuthorServiceImpl.class);
    private AuthorDao authorDao;

    public void setAuthorDao(AuthorDao authorDao) {
        this.authorDao = authorDao;
    }

    @Override
    public void saveOrUpdate(Author author, Book book) {
        if (author.getName().equals("")) return;
        if (book.getName().equals("")) return;

        Author a = authorDao.getByName(author.getName());
        if (a == null){
            List<Book> books = new ArrayList<>();
            books.add(book);
            author.setBooks(books);
            book.setAuthor(author);
            logger.info("try to save author");
            authorDao.save(author);

        } else {
            a.getBooks().add(book);
            book.setAuthor(a);
            logger.info("try to update author");
            authorDao.update(a);
        }
    }

    @Override
    public void delete(Author author) {
        if (author.getName().equals("")) return;

        Author a = authorDao.getByName(author.getName());
        if (a != null){
            authorDao.delete(a);
            logger.info("try to delete author");
        } else {
            logger.info("author not exist in db");
        }
    }


    @Override
    public List<Author> getAll() {
        return authorDao.getAll();
    }
}
