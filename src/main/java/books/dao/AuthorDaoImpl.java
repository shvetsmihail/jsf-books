package books.dao;

import books.model.Author;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

@Repository
public class AuthorDaoImpl implements AuthorDao {

    private static final Logger logger = Logger.getLogger(AuthorDaoImpl.class);
    private EntityManagerFactory entityManagerFactory;

    public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    @Override
    public void save(Author author) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(author);
        logger.info("Author saved successfully. " + author );
        entityManager.getTransaction().commit();
    }

    @Override
    public void update(Author author) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.merge(author);
        logger.info("Author update successfully. " + author );
        entityManager.getTransaction().commit();
    }

    @Override
    public void delete(Author author) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.remove(entityManager.merge(author));
        logger.info("Author deleted successfully. " + author );
        entityManager.getTransaction().commit();
    }

    @Override
    public List<Author> getAll() {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        List<Author> authors = entityManager.createQuery("select a from Author a").getResultList();
        logger.info("Author list get successfully.");
        return authors;
    }

    @Override
    public Author getByName(String name) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        Author author;
        try {
            author = (Author) entityManager.createQuery("select a from Author a where a.name = :name")
                                .setParameter("name", name).getSingleResult();
        } catch (Exception e){
            logger.info("Author not found");
            return null;
        }
        logger.info("Author get successfully.");
        return author;
    }
}
