package books.dao;


import books.model.Author;

import java.util.List;

public interface AuthorDao {
    Author getByName(String name);
    void save(Author author);
    void update(Author author);
    void delete(Author author);
    List<Author> getAll();

}
